window.addEventListener("DOMContentLoaded", () => {
  // This block will be executed once the page is loaded and ready

  const button = document.querySelector(".button");
  button.addEventListener("click", () => {
    alert("💣");
  });

  var avatar = document.querySelectorAll("div.image");
  avatar.forEach((element) => {
    element.addEventListener("click", () => {
      element.classList.add("active");
      element.style.transform = "scale(2)";
    });
  }); 
});
